<?php

/*
 * generator for bootstrap-gap.css and bootstrap-gap.min.css
 * the extensive arrays are there to help make modifications
 */

// file path of generated css file
// default /../dist/css/
$dirPath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'dist' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR;

// gap positions
$gapPositions = [
    'default' => [
        'name' => 'default',
        'rulePostfix' => ''
    ],
    'top' => [
        'name' => 'top',
        'rulePostfix' => '-top'
    ],
    'right' => [
        'name' => 'right',
        'rulePostfix' => '-right'
    ],
    'bottom' => [
        'name' => 'bottom',
        'rulePostfix' => '-bottom'
    ],
    'left' => [
        'name' => 'left',
        'rulePostfix' => '-left'
    ]
];

// gap types
$gapTypes = [
    'outer' => [
        'classPostfix' => '',
        'name' => 'outer',
        'rulePrefix' => 'margin',
        'ruleValuePrefix' => ''
    ],
    'inner' => [
        'classPostfix' => '-inner',
        'name' => 'inner',
        'rulePrefix' => 'padding',
        'ruleValuePrefix' => ''
    ],
    'overlap' => [
        'classPostfix' => '-overlap',
        'name' => 'overlap',
        'rulePrefix' => 'margin',
        'ruleValuePrefix' => '-'
    ]
];

// gap sizes
$gapSizes = [
//    'extraLarge' => [
//        'classPostfix' => '-xl',
//        'name' => 'extra large',
//        'ruleValue' => '60px'
//    ],
    'large' => [
        'classPostfix' => '-lg',
        'name' => 'large',
        'ruleValue' => '30px'
    ],
    'medium' => [
        'classPostfix' => '-md',
        'name' => 'medium',
        'ruleValue' => '20px'
    ],
    'default' => [
        'classPostfix' => '',
        'name' => 'default',
        'ruleValue' => '15px'
    ],
    'small' => [
        'classPostfix' => '-sm',
        'name' => 'small',
        'ruleValue' => '10px'
    ],
    'extraSmall' => [
        'classPostfix' => '-xs',
        'name' => 'extra small',
        'ruleValue' => '5px'
    ],
    'none' => [
        'classPostfix' => '-no',
        'name' => 'none',
        'ruleValue' => '0'
    ]
];

// gap classes
$gapClasses = [
    'default' => [
        'name' => 'default',
        'classPostfix' => '',
        'positions' => [
            'default'
        ]
    ],
    'top' => [
        'name' => 'top',
        'classPostfix' => '-top',
        'positions' => [
            'top'
        ]
    ],
    'right' => [
        'name' => 'right',
        'classPostfix' => '-right',
        'positions' => [
            'right'
        ]
    ],
    'bottom' => [
        'name' => 'bottom',
        'classPostfix' => '-bottom',
        'positions' => [
            'bottom'
        ]
    ],
    'left' => [
        'name' => 'left',
        'classPostfix' => '-left',
        'positions' => [
            'left'
        ]
    ],
//    'horizontal' => [
//        'name' => 'horizontal',
//        'classPostfix' => '-horizontal',
//        'positions' => [
//            'right',
//            'left'
//        ]
//    ],
    'vertical' => [
        'name' => 'vertical',
        'classPostfix' => '-vertical',
        'positions' => [
            'top',
            'bottom'
        ]
    ]
];

$gaps = [];

$counter = 0;

// loop through sizes
foreach ($gapSizes as $gapSizeKey => $gapSize) {
    // loop through types
    foreach ($gapTypes as $gapTypeKey => $gapType) {
        // prevent margin:-0;
        if ('none' == $gapSizeKey && 'overlap' == $gapTypeKey) continue;

        // loop through classes
        foreach ($gapClasses as $gapClassKey => $gapClass) {
            // prevent margin-top:-15px;margin-bottom:-15px;
            if ('overlap' == $gapTypeKey && 'vertical' == $gapClassKey) continue;

            if (0 != $counter) {
                $gaps[] = "\n\n";
            }

            // class like bootstrap:
            // col -sm -offset -top
            // gap -sm -inner  -top
            $gaps[] = '.gap';
            $gaps[] = $gapSize['classPostfix'];
            $gaps[] = $gapType['classPostfix'];
            $gaps[] = $gapClass['classPostfix'];
            $gaps[] = ' {';

            $positionsCount = count($gapClass['positions']);

            $positionsCounter = 1;

            // class can have multiple rules
            foreach ($gapClass['positions'] as $positionKey) {
                $gaps[] = "\n";
                $gaps[] = '    ';

                $gaps[] = $gapType['rulePrefix'];
                $gaps[] = $gapPositions[$positionKey]['rulePostfix'];
                $gaps[] = ':';
                $gaps[] = $gapType['ruleValuePrefix'];
                $gaps[] = $gapSize['ruleValue'];

                if ($positionsCounter != $positionsCount) {
                    $gaps[] = ';';
                    $positionsCounter++;
                }
            }

            $gaps[] = "\n";
            $gaps[] = '}';

            $counter++;
        }
    }
}

$cssContents = implode('', $gaps);

$files = [
    'default' => [
        'fileContents' => $cssContents,
        'fileName' => 'bootstrap-gap.css'
    ],
    'minified' => [
        'fileContents' => preg_replace("/\s+/", '', $cssContents),
        'fileName' => 'bootstrap-gap.min.css'
    ]
];

echo 'saving ' . $counter . ' classes to ' . count($files) . ' files';
echo "\n";

foreach ($files as $fileKey => $file) {
    $filePath = realpath($dirPath . $file['fileName']);

    echo $fileKey . ': ';

    if (@file_put_contents($filePath, $file['fileContents'])) {
        echo 'success';
    } else {
        echo 'failed';
    }

    echo ' (' . $filePath . ')';

    echo "\n";
}

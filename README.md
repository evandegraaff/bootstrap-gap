# bootstrap-gap

Simple generic classes named after Bootstrap's `col-*` classes to add or remove margin or padding to elements.

## Table of contents

* [Quick start](#quick-start)
* [Documentation](#documentation)
* [Customize](#customize)
* [Versioning](#versioning)

## Quick start

* Clone the repository: `git clone https://github.com/evangraaff/bootstrap-gap`.
* Install with [Bower](http://bower.io): `bower install bootstrap-gap --save`.

Include the `bootstrap-gap.min.css` stylesheet.

```
<link rel="stylesheet" href="bootstrap-gap.min.css">
```

## Documentation

### Usage

Now that you have included the stylesheet, you can add or remove gaps by adding a class structured as following.
                                         
```
gap [-lg] [-inner] [-top     ]
      md             right
      sm             bottom
      xs             left
      no             vertical
```

### Positions

size      | value
--------- | -----
*default* | *shorthand*
top       | top
right     | right
bottom    | bottom
left      | left
vertical  | *top and bottom*

### Sizes

size                                      | value
----------------------------------------- | -----
lg                                        | 30px
md                                        | 20px
*default*                                 | 15px
sm                                        | 10px
xs                                        | 5px
no *(not available when type is overlap)* | 0

### Types

type      | value
--------- | -----
*default* | margin
inner     | padding
overlap   | negative margin

## Customize

You can easily customize this stylesheet by editing and running the `php/generate.php` PHP script. You need to have PHP >= 5.4 installed.

## Versioning

Bootstrap-gap is maintained under [the Semantic Versioning guidelines](http://semver.org/).